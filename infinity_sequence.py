def infinity_sequence():
    num = 0
    while True:
        yield num
        num += 1


def study_yield():
    yield "first"
    yield "second"
    yield "third"

nums_squared_lc = [i ** 2 for i in range(1000000)]

nums_squared_gc = (i ** 2 for i in range(1000000))