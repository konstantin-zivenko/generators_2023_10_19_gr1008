def csv_reader(file_name):
    file = open(file_name)
    result = file.read().split("\n")
    return result


csv_gen = ()

csv_gen = csv_reader("some_csv.txt")

for row in csv_gen:
    ...

csv_gen = (row for row in open(file_name))
