def is_palindrome(num):
    if num // 10 == 0:
        return False
    temp = num
    reversed_num = 0

    while temp != 0:
        reversed_num = (reversed_num * 10) + (temp % 10)
        temp = temp // 10

    if num == reversed_num:
        return num
    else:
        return False


if __name__ == "__main__":
    from infinity_sequence import infinity_sequence

    for i in infinity_sequence():
        pal = is_palindrome(i)
        if pal:
            print(i)